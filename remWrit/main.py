import googletrans
import colorful as cf
import re, os, pickle
from pathlib import Path


from .helpers import srt as srt_helper

class RemWrit:

    QUIT = -2
    PASS = -1
    FAILED = 0
    SUCCESS = 101
    REPEAT = 102

    def __init__(self, srt_path, encoding='cp1252'):
        self.srt_path = srt_path
        self.pickle_path = f'{srt_path}.pickle'

        if Path(self.pickle_path).is_file():
            self.srt = pickle.load(open(self.pickle_path, 'rb'))
        else:
            self.srt = srt_helper.load(srt_path, encoding=encoding)

        T = googletrans.Translator()
        self.T = T.translate
        self.target_lang = 'en'

    def translate(self, text):
        return self.T(text).text

    def _clear(self):
        os.system('cls' if os.name == 'nt' else 'clear')

    def _text_to_words(self, text):
        return re.sub(r'[^\w|\s]', '', text).lower().split(' ')

    def _compare_texts(self, text1, text2):
        words1 = self._text_to_words(text1)
        words2 = self._text_to_words(text2)

        corrects = 0

        for iw in range(min(len(words1), len(words2))):
            w1, w2 = words1[iw], words2[iw]

            for i in range(min(len(w1), len(w2))):
                if w1[i] == w2[i]:
                    corrects += 1

        n_chars = sum([len(x) for x in words1])
        return 100 * corrects / n_chars

    def play_one(self, text):
        translation = self.translate(text)
        print(cf.bold_green('- ' + text + ' [' + translation + ']'))

        action = input('What to do [p: pass, r: ready, q: quit]: ')

        self._clear()

        if action == 'p':
            return self.PASS, None
        elif action == 'r':
            print(f'Your turn\n[hint: {translation}]')
            user_input = input('')
            accuracy = self._compare_texts(text, user_input)
            return self.SUCCESS, accuracy
        elif action == 'q':
            return self.QUIT, None
        else:
            return self.REPEAT, None



    def play(self, criteria={'perfect': 95, 'pass': 75}):
        i = 1
        while True:
            if self.srt[i]['done']:
                i += 1
                continue

            text = '. '.join(self.srt[i]['content'])

            status, accuracy = self.play_one(text)

            if status == self.QUIT:
                break
            elif status == self.PASS:
                self.srt[i]['done'] = True
                i += 1
            elif status == self.SUCCESS:
                if accuracy > criteria['perfect']:
                    print(cf.bold_green(
                        f'Perfect! You\'v reached a {int(accuracy)}% accuracy.'
                    ))

                    self.srt[i]['done'] = True
                    self.srt[i]['accuracy'] = int(accuracy)
                    i += 1
                elif accuracy > criteria['pass']:
                    print(cf.bold_green(
                        f'You passed this test! Here\'s your accuracy: {int(accuracy)}'
                    ))
                    self.srt[i]['done'] = True
                    self.srt[i]['accuracy'] = int(accuracy)
                    i += 1
                else:
                    print(cf.bold_red(
                        f'You\'ve made too many mistakes, try again!'
                    ))
            elif status == self.REPEAT:
                pass
            else:
                pass

            pickle.dump(self.srt, open(self.pickle_path, 'wb'))
