import re, datetime


def load(path, encoding='cp1252'):
    f = open(path, encoding=encoding)
    lines = f.readlines()
    f.close()

    result = {}

    i = 0

    for line in lines:
        line = line.replace('\n', '')
        if len(line) < 1:
            continue

        if _is_int(line):
            i = int(line)
            result[i] = {
                'content': [],
                'done': False,
                'accuracy': 0,
            }
        elif _is_timestamp(line.split(' ')[0]):
            result[i]['time'] = {
                'start': _str_to_timestamp(line.split(' ')[0]),
                'end': _str_to_timestamp(line.split(' ')[-1]),
            }
        else:
            result[i]['content'].append(
                _clean_text(_remove_html_tags(line))
            )


    return result


def _is_int(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def _str_to_timestamp(string):
    return datetime.datetime.strptime(string, '%H:%M:%S,%f')

def _is_timestamp(string):
    try:
        datetime.datetime.strptime(string, '%H:%M:%S,%f')
        return True
    except ValueError:
        return False

def _remove_html_tags(string):
    reg = re.compile('<.*?>')
    return re.sub(reg, '', string)

def _clean_text(string):
    return string.replace('- ', '').strip()
