import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="remWrit",
    version="0.0.0",
    author="Saeed Sarpas",
    author_email="s.sarpas@gmail.com",
    description="Practice your writing skills!",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/remWrit/remWrit.git",
    packages=['remWrit'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU GENERAL PUBLIC LICENSE",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.0',
    scripts=[
        'scripts/remWrit_it',
    ],
    install_requires=[
        'googletrans',
        'colorful',
    ],
)
